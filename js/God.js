(function ($, window, document, undefined) {
    'use strict';

    var God,
        defaults = {
            color: colors.white,
            size: 50,
            id: 'god',
            counter: 0,
            type: 'god'
        };

    God = function (settings) {
        $.extend(this, defaults, settings);
        Entity.call(this);
    };

    // inherit from Entity
    God.prototype = new Entity();
    God.prototype.constructor = God;
    
    $.extend(God.prototype, {

        init: function () {
            // track time for timeout
            this.time = Date.now();
            this.setDelay();
            // lightening bolt container
            this.entities = [];
        },

        throwLighteningBolt: function () {
            this.entities.push(new LighteningBolt({
                id: 'lightening-bolt-' + this.counter++,
                position: this.mesh.position
            }));
        },

        update: function () {
            var i, j,
                now = Date.now(),
                delta = now - this.time;

            if (delta >= this.delay) {
                this.throwLighteningBolt();
                this.time = now;
                this.setDelay();
            }

            // update entities
            for (i = 0, j = this.entities.length; i < j; i++) {
                if (this.entities[i].updatable === true) {
                    if (this.entities[i].mesh.position.y < -113) {
                        return this.removeEntity(this.entities[i].id);
                    } else {
                        this.entities[i].update();
                    }
                }
            }
        },

        setDelay: function () {
            this.delay = (Math.random() * 75) + 10;
        }

    });

    window.God = God;

})($, window, document);