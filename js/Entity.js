(function ($, window, document, undefined) {
    'use strict';

    var Entity;

    Entity = function () {
        this.timeouts = {};
        this.Geometry = this.Geometry || THREE.SphereGeometry;
        this.Material = this.Material || THREE.MeshLambertMaterial;
        this.Mesh = this.Mesh || THREE.Mesh;
        this.updatable = false;
        if (typeof this.scene !== 'undefined') {
            if (typeof this.loadModel === 'function') {
                this.loadModel().then($.proxy(function (mesh) {
                    this.mesh = this.createModel(mesh);
                    this.updatable = true;
                    this.init();
                }, this));
            } else {
                // generate the object mesh (mesh) and add it to the scene
                this.mesh = this.createModel();
                this.updatable = true;
                this.init();
            }
        }
    };
    
    Entity.prototype = {

        createModel: function () {
            var geometry, material, mesh;

                        if(this.id === 'god') {
                                geometry = new this.Geometry(
                    this.width,
                    this.height,
                    this.depth
                );
                material = new this.Material({
                    color: this.color,
                    transparent: true,
                    opacity: 0
                });

            } else if (this.Geometry === THREE.CubeGeometry) {
                geometry = new this.Geometry(
                    this.width,
                    this.height,
                    this.depth
                );
                material = new this.Material({
                    color: this.color,
                    transparent: true,
                    opacity: this.opacity
                });
            } else {
                geometry = new this.Geometry(
                    this.size,
                    6,
                    6
                );
                material = new this.Material({
                    color: this.color
                });
            }

            mesh = new this.Mesh(geometry, material);

            // make updatable
            mesh.geometry.dynamic = true;
            mesh.geometry.verticesNeedUpdate = true;
            mesh.geometry.normalsNeedUpdate = true;

            // set lighting
            mesh.receiveShadow = true;
            mesh.castShadow = true;

            // set name and type in order to be able to identify it
            mesh.name = this.id;
            mesh.type = this.type;

            this.scene.add(mesh);

            this.meshes.push(mesh);

            return mesh;
        },

        destroy: function () {
            this.removeMesh();
            // console.log(this.id, 'was destroyed.');
        },

        removeMesh: function () {
            var i, j;
            this.scene.remove(this.mesh);
            for (i = 0, j = this.meshes.length; i < j; i++) {
                if (this.meshes[i].name === this.id) {
                    this.meshes.splice(i, 1);
                    return true;
                }
            }
        },

        getEntity: function (id) {
            var i, j;
            for (i = 0, j = this.entities.length; i < j; i++) {
                if (this.entities[i].id === id) {
                    return this.entities[i];
                }
            }
        },

        removeEntity: function (id) {
            var i, j;
            for (i = 0, j = this.entities.length; i < j; i++) {
                if (this.entities[i].id === id) {
                    this.entities[i].destroy();
                    this.entities.splice(i, 1);
                    return true;
                }
            }
        }

    };

    window.Entity = Entity;

})($, window, document);