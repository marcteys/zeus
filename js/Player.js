(function ($, window, document, undefined) {
    'use strict';

    var Player,
        defaults = {
            color: colors.blue,
            size: 10,
            type: 'player',
            score: 50,
            speed: {
                x: 1
            },
            walking: true,
            duration: 1000,
            keyframes: 28,
            lastKeyframe: 0,
            currentKeyframe: 0
        };

    Player = function (settings) {
        $.extend(this, defaults, settings);
        this.score = 50;
        this.interpolation = this.duration / this.keyframes;
        $.extend(this.controls, {
            keyboard: true,
            gamepad: false
        });
        Entity.call(this);
    };

    Player.prototype = new Entity();
    Player.prototype.constructor = Player;

    $.extend(Player.prototype, {
    
        init: function () {
            if (this.id === 'player1') {
                this.mesh.position.set(-200, -110, 0);
            } else {
                this.mesh.position.set(200, -110, 0);
            }
        },

        // loadModel: function () {
        //     var loader = new THREE.JSONLoader(),
        //         promise = $.Deferred();

        //     loader.load('js/objects/characterRun.js', $.proxy(function (geometry, materials) {
        //         var i, j, mesh, material;

        //         // for preparing animation
        //         for (i = 0, j = materials.length; i < j; i++) {
        //             materials[i].morphTargets = true;
        //         }

        //         material = new THREE.MeshPhongMaterial({
        //             // light
        //             specular: '#a9fcff',
        //             // intermediate
        //             color: '#ebc90c',
        //             // dark
        //             emissive: '#f1ad0a',
        //             shininess: 60
        //         });

        //         mesh = new THREE.Mesh(geometry, material);

        //         mesh.scale.set(50,50,50);

        //         console.log('HELLO');

        //         promise.resolve(mesh);
        //     }, this));

        //     return promise;
        // },

        // createModel: function (mesh) {
        //     // set name and type in order to be able to identify it
        //     mesh.name = this.id;
        //     mesh.type = this.type;
        //     this.scene.add(mesh);
        //     this.meshes.push(mesh);
        //     return mesh;
        // },

        update: function () {

            var time = Date.now() % this.duration,
                keyframe;
       
            if (this.controls.gamepad === true) {

                if (this.mesh.position.x > this.boundingBox.start && this.mesh.position.x < this.boundingBox.finish) {
                    this.mesh.position.x += this.speed.x;
                }

            } else if (this.controls.keyboard === true) {
            
                // keyboard controls
                if (this.keyboard.pressed(this.controls.left) && this.mesh.position.x > this.boundingBox.start) {
                    this.mesh.position.x -= 7;
                }
                if (this.keyboard.pressed(this.controls.right) && this.mesh.position.x < this.boundingBox.finish) {
                    this.mesh.position.x += 7;
                }

            }

            // keyframe = Math.floor( time / this.interpolation ) + this.animOffset;

            // if (keyframe !== this.currentKeyframe) {
            //     this.mesh.morphTargetInfluences[ this.lastKeyframe ] = 0;
            //     this.mesh.morphTargetInfluences[ this.currentKeyframe ] = 1;
            //     this.mesh.morphTargetInfluences[ keyframe ] = 0;
            //     this.lastKeyframe = this.currentKeyframe;
            //     this.currentKeyframe = keyframe;
            // }

            // this.mesh.morphTargetInfluences[ keyframe ] = ( time % this.interpolation ) / this.interpolation;
            // this.mesh.morphTargetInfluences[ this.lastKeyframe ] = 1 - this.mesh.morphTargetInfluences[ keyframe ];

        },

        kill: function () {
        
            this.score = this.score - 2;
            
            if (this.score === 0 || this.score < 0) {
                $("#scores-" + this.id).addClass("loser");
                if (this.id === "player1") {
                    $("#alert").html("Player 2 Wins!").css({opacity: 1}).addClass("player2");
                } else if (this.id === "player2") {
                    $("#alert").html("Player 1 Wins!").css({opacity: 1}).addClass("player1");
                }
                setTimeout(function() {location.reload()}, 3000);
            } else {
                $("#scores-" + this.id).css({width: this.score + "%"}); 
                
                // console.log(this);
                
                var hitLight = new THREE.AmbientLight( this.color, 10, 40 );
                    hitLight.position.y = this.mesh.position.y;
                    hitLight.position.x = this.mesh.position.x;
                    hitLight.position.z = 30;
                this.scene.add( hitLight );

                setTimeout($.proxy(function () {
                    this.scene.remove( hitLight );
                }, this), 100);
            }
            
        }

    });

    window.Player = Player;

})($, window, document);
