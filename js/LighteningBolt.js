(function ($, window, document, undefined) {
    'use strict';

    var LighteningBolt,
        defaults = {
            color: colors.yellow,
            size: 5,
            type: 'lightening-bolt'
        };

    LighteningBolt = function (settings) {
        $.extend(this, defaults, settings);
        this.speed = {
            x: (Math.floor(Math.random() * 19) - 9) / 10,
            y: 1
        };
        Entity.call(this);
    };

    // inherit from Entity
    LighteningBolt.prototype = new Entity();
    LighteningBolt.prototype.constructor = LighteningBolt;
    
    $.extend(LighteningBolt.prototype, {

        init: function () {
            this.mesh.position.x = this.position.x;
            this.mesh.position.y = this.position.y;
            this.setRotation();
        },

        setRotation: function () {
            var rotation, axis;
            rotation = new THREE.Matrix4();
            axis = new THREE.Vector3(0,0,1);
            rotation.makeRotationAxis(axis, (this.speed.x / 0.9) * (Math.PI / 4) - Math.PI / 12);
            this.mesh.matrix.multiply(rotation);
            this.mesh.rotation.setFromRotationMatrix(this.mesh.matrix);
        },

        loadModel: function () {
            var loader = new THREE.JSONLoader(),
                promise = $.Deferred();

            loader.load('js/objects/thunderbolt.js', $.proxy(function (geometry) {
                var mesh, material, rotation, axis, lightsource;

                material = new THREE.MeshPhongMaterial({
                    // light
                    specular: '#a9fcff',
                    // intermediate
                    color: '#ebc90c',
                    // dark
                    emissive: '#f1ad0a',
                    shininess: 60
                });

                mesh = new THREE.SkinnedMesh(
                    geometry,
                    material
                );
                mesh.scale.set(1,1,1);

         


                promise.resolve(mesh);
            }, this));

            return promise;
        },

        createModel: function (mesh) {
            // set name and type in order to be able to identify it
            mesh.name = this.id;
            mesh.type = this.type;
            this.scene.add(mesh);
            this.meshes.push(mesh);
            return mesh;
        },

        update: function () {
            this.mesh.position.x += this.speed.x * 2.5;
            this.mesh.position.y -= this.speed.y * 2.5;
        }

    });

    window.LighteningBolt = LighteningBolt;

})($, window, document);