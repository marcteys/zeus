(function ($, window, document, undefined) {

    var colors = {
        blue: 0x4076E1,
        red: 0xD81313,
        white: 0xFFFFFF,
        yellow: 0xFFFF00
    };

    window.colors = colors;

})($, window, document);