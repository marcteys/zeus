(function ($, window, document, undefined) {
    'use strict';

    var Rain,
        defaults = {
            color: 0xFFFFFF,
            opacity: Math.random() * (0.4 - 0),
            width: 0.5,
            height: Math.floor((Math.random()*100)+20),
            depth: 1,
            Geometry: THREE.CubeGeometry
        };

    Rain = function (settings) {
        $.extend(this, defaults, settings);
        this.speed = {
            x: -2,
            y: -2
        };
        Entity.call(this);
        this.init();
    };

    // inherit from Entity
    Rain.prototype = new Entity();
    Rain.prototype.constructor = Rain;
    
    $.extend(Rain.prototype, {

        init: function () {
            // position the mesh
            this.mesh.scale.y = Math.random() + 0.4;
            this.mesh.position.set(Math.floor(Math.random() * 1001) - 500, 150, 400);
            this.mesh.rotation.z = -Math.PI / 4;
            this.mesh.material.opacity = (Math.random() * 0.2) + 0.1;
            
        },
        
        update: function () {
            this.mesh.position.x += this.speed.x;
            this.mesh.position.y += this.speed.y;
            // remove lightening bolt if it's off camera or has been on the ground for too long
        }

    });

    window.Rain = Rain;

})($, window, document);