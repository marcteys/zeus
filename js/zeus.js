(function ($, window, document, undefined) {
    'use strict';

    var Zeus,
        defaults = {
            container: '#container',
            width: 400,
            height: 300,
            viewingAngle: 45,
            scaleFactor: 6,
            near: -500,
            far: 2000
        };
         
    Zeus = function (settings) {
        settings = settings || {};
        this.settings = $.extend({}, defaults, settings);
        if (typeof this.settings.aspectRatio === 'undefined') {
            this.settings.aspectRatio = this.settings.width / this.settings.height;
        }
        this.settings.left = this.settings.width / -this.settings.scaleFactor;
        this.settings.right = this.settings.width / this.settings.scaleFactor;
        this.settings.top = this.settings.height / this.settings.scaleFactor;
        this.settings.bottom = this.settings.height / -this.settings.scaleFactor;

        this.$container = $(this.settings.container);
        this.init();
    };

    Zeus.prototype = {

        init: function () {

            // init main components
            this.renderer = new THREE.WebGLRenderer({
                antialias: true
            });

            this.camera = new THREE.OrthographicCamera(
                this.settings.left,
                this.settings.right,
                this.settings.top,
                this.settings.bottom,
                this.settings.near,
                this.settings.far
            );

            this.scene = new THREE.Scene();
            this.keyboard = new THREEx.KeyboardState();
            this.meshes = [];
             this.scene.fog = new THREE.Fog(0x11171f, 0, .85);

            // delay collision detection
            setTimeout($.proxy(function () {
                this.detectCollisions = true;
            }, this), 1000);

            // pass references to child instances
            $.extend(Entity.prototype, {
                scene: this.scene,
                keyboard: this.keyboard,
                meshes: this.meshes,
                boundingBox: {
                    start: -200,
                    finish: 200
                }
                // entities: this.entities
            });

            // add origin arrows
            // this.addOriginArrows();

            // entities to keep track of stuff in order to be able to update
            this.entities = [];

            this.initPlayers();
            this.initGod();

            this.cloud = new Cloud();
            this.entities.push(this.cloud);
            
            this.guiInit();

            // attach camera to scene and set its z position
            this.scene.add(this.camera);
            this.camera.position.z = 300;

            // set render size
            this.renderer.setSize(this.settings.width, this.settings.height);

            // append threejs context to DOM
            this.$container.append(this.renderer.domElement);

            this.plane = this.createPlane();
            this.plane.rotation.x = Math.PI / -2;
            this.scene.add(this.plane);
            
            // add light source
            this.addLightSource({
                x: 10,
                y: 50,
                z: 130
            });

            //add the mountains
            this.addMountains();

            //clouds
            this.addCloud();


            //clouds
            this.addZeusCloud();

            // Deep Ambient Shit
            
            this.initRain();
            
            // deep ambient shit
            var directionalLight = new THREE.DirectionalLight(0x192229, 1);
            directionalLight.position.set(5, 4, 10);
            this.scene.add(directionalLight);


            // start the draw loop
            this.loop();

            console.log('Hello World!', this);
        },

        initGod: function () {
            this.god = new God();
            this.entities.push(this.god);       
            this.god.mesh.position.set(0, 100, 0);
        },
        
        initRain: function() {
            this.rain = new Rain();
            this.entities.push(this.rain);  
        },

        initPlayers: function () {
            this.player1 = new Player({
                id: 'player1',
                color: colors.blue,
                controls: {
                    left: 'a',
                    right: 'd'
                },
                controller: 0
            });

            this.player2 = new Player({
                id: 'player2',
                color: colors.red,
                controls: {
                    left: 'left',
                    right: 'right'
                },
                controller: 1
            });

            this.players = [this.player1, this.player2];
            this.entities.push(this.player1);
            this.entities.push(this.player2);
        },

        addOriginArrows: function () {
            var origin = new THREE.Vector3(0, 0, 0);

            this.scene.add(new THREE.ArrowHelper(new THREE.Vector3(1, 0, 0), origin, 25, 0xDC0000));
            this.scene.add(new THREE.ArrowHelper(new THREE.Vector3(0, 1, 0), origin, 25, 0x1C1CDF));
            this.scene.add(new THREE.ArrowHelper(new THREE.Vector3(0, 0, 1), origin, 25, 0x00E800));
        },

        addMountains: function () {

            var dae, skin, montainMaterial,
                that = this,
                loader = new THREE.ColladaLoader();

            loader.options.convertUpAxis = true;

            loader.load( './js/objects/montains.dae', function ( collada ) {

                dae = collada.scene;
                skin = collada.skins[ 0 ];

                dae.scale.x = dae.scale.y = dae.scale.z = 50;
                dae.rotation.y = -90*Math.PI / 180
                dae.updateMatrix();

                dae.position.y = -120;
                dae.position.z = -600;
                
                montainMaterial = new THREE.MeshLambertMaterial({ color: 0x21262c });
                montainMaterial.ambient.setHex( 0x000000 );
                dae.children[ 0 ].material = montainMaterial ;

                that.scene.add(dae);

            });
        },
               addZeusCloud: function () {

            var dae, skin, montainMaterial,
                that = this,
                loader = new THREE.ColladaLoader();

            loader.options.convertUpAxis = true;



            var cloudmat =   new THREE.MeshPhongMaterial({
                    color: '#999999',
                    emissive: '#888888',
                    transparent : true,
                    opacity: 0.8
                  });




            loader.load( './js/objects/zeuscloud.dae', function ( collada ) {

  

                dae = collada.scene;
                skin = collada.skins[ 0 ];

                dae.scale.x = dae.scale.y = dae.scale.z = 8;
                dae.updateMatrix();

                dae.position.y = 100;
               // dae.position.z = -920;
                
                dae.children[ 0 ].material = new THREE.MeshPhongMaterial({
                    color: '#999999',
                    emissive: '#888888',
                    transparent : true,
                    opacity: 0.8
                  });
                that.scene.add(dae);

            });



        },


         addCloud: function () {

                var i;
                var dae, skin, montainMaterial;
                var that = this;
                var loader = new THREE.ColladaLoader();


              var cloudmat =   new THREE.MeshPhongMaterial({
                    // light
                 //   specular: '#ffffff',
                    // intermediate
                    color: '#999999',
                    // dark
                    emissive: '#888888',
                   // shininess: 200 ,
                    transparent : true,
                    opacity: 0.8
                  });


                loader.options.convertUpAxis = true;
                loader.load( './js/objects/cloud1.dae', function ( collada ) {

                  for(i = 0; i<collada.scene.children.length;i++ ) {

                    dae = collada.scene.children[i];


                    dae.scale.x = dae.scale.y = dae.scale.z = 5;
                    dae.rotation.y = -90*Math.PI / 180
                    dae.updateMatrix();

                    dae.position.x = Math.floor((Math.random()*400-200) / 20) * 20;
                    dae.position.y = Math.random()*40- -30;


                    that.scene.add(dae);
                    dae.material = cloudmat;


              }

            });




        },
        createPlane: function () {
            var geometry, material;

            geometry = new THREE.PlaneGeometry(150, 150);

            material = new THREE.MeshBasicMaterial({ opacity: 0, transparent: true });

            return new THREE.Mesh(geometry, material);
        },

        addLightSource: function (position) {

            var hemiLight, dirLight;

            hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.8 );
            hemiLight.color.setHSL( 0.6, 0.8, 0.7 );
            hemiLight.groundColor.setHSL( 0.075, 1, 0.75 );
            hemiLight.position.set( 0, 500, 0 );
            this.scene.add( hemiLight );

            dirLight = new THREE.DirectionalLight( 0x6b858e, 1 );
            dirLight.position.set( 0.2, 0.75, -1 );
            dirLight.position.multiplyScalar( 60 );
            this.scene.add( dirLight );

            var light = new THREE.PointLight( 0x121416, 100, 200 );
            light.position.y = 100;
            light.position.x = 0;
            light.position.z = -400;
            this.scene.add( light );


          var light = new THREE.PointLight( 0xfeff9e, 20, 2000 );
            light.position.y = 100;
            light.position.x = 0;
            light.position.z = -900;
            this.scene.add( light );



          var light = new THREE.PointLight( 0xe9d103, 10, 50 );
            light.position.y = 100;
            light.position.x = 0;
            light.position.z = -900;
            this.scene.add( light );

          var light = new THREE.PointLight( 0xe9d103, 1, 40 );
            light.position.y = 75;
            light.position.x = 0;
            light.position.z = 20;
          this.scene.add( light );


        },

        loop: function () {
            requestAnimationFrame($.proxy(this.loop, this));
            this.update();
            this.draw();
        },

        update: function () {
            var i, j, x, y, player, collidables, originPoint, localVertex, globalVertex, directionVector, ray, collisionResults;
            // update entities
            for (i = 0, j = this.entities.length; i < j; i++) {
                if (this.entities[i].updatable === true) {
                    this.entities[i].update();
                }
            }
            // collision detection on players
            if (this.detectCollisions === true) {
                for (i = 0, j = this.players.length; i < j; i++) {
                    player = this.players[i];
                    // collision detection
                    collidables = this.getCollidables(player.id);
                    originPoint = player.mesh.position.clone();
                    for (x = 0, y = player.mesh.geometry.vertices.length; x < y; x++) {
                        localVertex = player.mesh.geometry.vertices[x].clone();
                        globalVertex = localVertex.applyMatrix4(player.mesh.matrix);
                        directionVector = globalVertex.sub(player.mesh.position);
                        ray = new THREE.Raycaster(originPoint, directionVector.clone().normalize());
                        collisionResults = ray.intersectObjects(this.meshes);
                        if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length()) {
                            if (collisionResults[0].object.type === 'lightening-bolt') {
                                this.god.removeEntity(collisionResults[0].object.name);
                                player.kill();
                                return false;
                            }
                        }
                    }
                }
            }
        },

        draw: function () {
            // render the scene
            this.renderer.render(this.scene, this.camera);
        },

        getCollidables: function (id) {
            var i, j,
                collidables = [];
            for (i = 0, j = this.meshes.length; i < j; i++) {
                if (this.meshes[i].name !== id) {
                    collidables.push(this.meshes[i]);
                }
            }
            return collidables;
        },
        
        getEntity: function (id) {
            var i, j;
            for (i = 0, j = this.entities.length; i < j; i++) {
                if (this.entities[i].id === id) {
                    return this.entities[i];
                }
            }
        },

        removeEntity: function (id) {
            var entity = this.getEntity(id);
            entity.destroy();
        },
        
        guiInit: function() {
            $("#scores-player1").width(this.player1.score + "%");
            $("#scores-player2").width(this.player2.score + "%");
        }
            
    };

    // expose Zeus object to global scope (window)
    window.Zeus = Zeus;

})($, window, document);