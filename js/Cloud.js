(function ($, window, document, undefined) {
    'use strict';

    var Cloud,
        defaults = {
            id: 'cloud',
            counter: 0,
            type: 'cloud'
        };

    Cloud = function (settings) {
        $.extend(this, defaults, settings);
        Entity.call(this);
    };

    // inherit from Entity
    Cloud.prototype = new Entity();
    Cloud.prototype.constructor = Cloud;
    
    $.extend(Cloud.prototype, {

        init: function () {
            // track time for timeout
            this.time = Date.now();
            this.setDelay();
            // rain container
            this.entities = [];
        },

        createModel: function () {},

        makeRain: function () {
            this.entities.push(new Rain({
                id: 'rain-' + this.counter++
            }));
        },

        update: function () {
            var i, j,
                now = Date.now(),
                delta = now - this.time;

            if (delta >= this.delay) {
                this.makeRain();
                this.time = now;
                // this.setDelay();
            }

            // update entities
            for (i = 0, j = this.entities.length; i < j; i++) {
                if (this.entities[i].updatable === true) {
                    if (this.entities[i].mesh.position.y < -120) {
                        return this.removeEntity(this.entities[i].id);
                    } else {
                        this.entities[i].update();
                    }
                }
            }
        },

        setDelay: function () {
            this.delay = 50;
        }

    });

    window.Cloud = Cloud;

})($, window, document);